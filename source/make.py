import json
import os
import yatl
from yatl.helpers import XML
import yaml

BASEDIR = "source/templates"

HELPERS = {name: getattr(yatl.helpers, name) for name in yatl.helpers.__all__}

def main():
    _, _, filenames = next(os.walk(BASEDIR))
    # Produces each file. 
    for fn in filenames:
        if not fn.startswith("_"):
            fname, fext = fn.split(".")
            if fext == "yatl":
                # Renders the web page. 
                # Reads the variable defs, if any.
                yaml_path = os.path.join(BASEDIR, ".".join([fname, "yaml"]))
                if os.path.isfile(yaml_path):
                    with open(yaml_path) as f:
                        vs = yaml.safe_load(f)
                else:
                    vs = {}
                # Adds the HTML helpers. 
                vs.update(HELPERS)
                html_path = ".".join([fname, "html"])
                print("Writing to:", html_path)
                template_path = os.path.join(BASEDIR, fn)
                with open(template_path) as tfile:
                    with open(html_path, "w") as html_file:
                        html_file.write(yatl.render(stream=tfile, path=BASEDIR, 
                                                    context=vs, delimiters="[[ ]]"))
    
    
main()
