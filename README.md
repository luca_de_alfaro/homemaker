# HomeMaker

This repo enables you to make static web pages, dynamically! 

Ever wanted to create _static_ web pages (so that even lame search engines can index them easily), in a _dynamic_ way, so that: 

* the pages of your site can all share a common layout that you define once and forall, 
* you can easily specify long lists of content that are then automatically formatted?

This is what the code in this repo enables you to do. 

## How to use

First, clone the repo.  Then, make sure you have the dependencies installed; these are listed in `requirements.txt` and are:  

 * PyYAML : yaml is used to specify page contents programmatically
 * [yatl](https://github.com/web2py/yatl), the templating language used by [web2py](http://web2py.com) and [py4web](https://py4web.com), which is documented [here](http://web2py.com/books/default/chapter/29/05/the-views). 

Then, edit the templates.  The templates are in the `source/templates` directory.  The templates direct how the web pages are created: 

* A template whose name begins with an underscore `_` is assumed to be a general page template, and is not translated in a page. 
* A template whose name starts with something else, e.g., `page.yatl`, is used to produce the page `page.html`.  To produce `page.html`: 
  * First, the definitions in `page.yaml` are read, 
  * then, the template `page.yatl` is interpreted, generating the page `page.html` at top level. 

Once you are done editing the templates, produce the HTML pages via: 

    python source/make.py

## An example: producing your home page

As an example, if you wish to produce your home page, proceed as follows. 

* Edit `source/templates/_base.yatl`.  This is the base template for the page, and you can customize the general layout, your image (should you find the one provided not to your liking), the tabs (you can add more tabs to About and Publications), and more. 
* Edit `source/templates/index.yatl` and `source/templates/index.yaml`.  This is the page that, using the above `_base.yatl` template, will be the index of the site -- the page that typically shows up by default. 
* Edit `source/templates/publications.yatl` and `source/templates/publications.yaml` to create the list of your publications.  In fact, you can likely leave `publications.yatl` unchanged, and just provide the list of your papers, remembering also to put the files in the corresponding `papers/` folder.  The list of papers is then generated and styled automatically from your `yaml` specification. 

Once the above is done, 

    python source/make.py

will make the pages at top level.  Once the pages are there, you can easily deploy on sites that can host your web pages, such as [bitbucket.io](https://support.atlassian.com/bitbucket-cloud/docs/publishing-a-website-on-bitbucket-cloud/).

## Going Forward

The [yatl](https://github.com/web2py/yatl) template language is quite powerful, and you should have no trouble modifying the templates to obtain any pages you want.  Enjoy!
